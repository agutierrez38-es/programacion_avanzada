/**
 * Hola mundo en Java
 * Codigo en java
 * @author Andrea Gutierrez Blanco
 */
public class HolaMundo {
  public static void main (String[] args) {
    String naranja = "\033[33m";
    String azul = "\033[34m";
    System.out.println(naranja + "Hola" + azul + " mundo");
  }
}
